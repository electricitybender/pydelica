import pytest
import os
import logging
import platform

from pydelica.compiler import Compiler

logging.getLogger("PyDelica").setLevel(logging.DEBUG)


@pytest.fixture(scope="module")
def compile_model():
    _sfode_src = os.path.join(
        os.path.dirname(__file__), "models", "SineCurrent.mo"
    )
    _compiler = Compiler()
    _compiler._library.use_library("Modelica", "3.2.3")
    _compiler._library.use_library("ModelicaServices", "3.2.3")
    _compiler._library.use_library("Complex", "3.2.3")
    return _compiler.compile(_sfode_src)


@pytest.mark.compilation
def test_binary(compile_model):
    if platform.system() == "Windows":
        assert os.path.exists(
            os.path.join(compile_model, "SineCurrentModel.exe")
        )
    else:
        assert os.path.exists(os.path.join(compile_model, "SineCurrentModel"))
